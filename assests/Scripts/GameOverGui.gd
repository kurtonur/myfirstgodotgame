extends CanvasLayer

onready var GameNode = Variables.GameNode
onready var PlayNode = Variables.PlayNode
onready var MenuNode = Variables.MenuNode

func _ready():
	if(Config.HighScore != Variables.Score):
		$menu/CenterContainer/hScore.set("custom_colors/font_color",Color(75,0,0))
		$menu/CenterContainer/hScore.text = "Your Score\n" + String(Variables.Score)
	else:
		$menu/CenterContainer/hScore.set("custom_colors/font_color",Color(0,75,0))
		$menu/CenterContainer/hScore.text = "New High Score\n" + String(Variables.Score)
	pass

func _on_start_button_down():
	if(Config.Sound):
		Variables.GameNode.get_node("Sound/soundButton").play()
		
	GameNode.removeScene(PlayNode);
	GameNode.addScene(PlayNode,GameNode.playActive);
	GameNode.set_gameState(1)
	queue_free()
	pass

func _on_Menu_button_down():
	if(Config.Sound):
		Variables.GameNode.get_node("Sound/soundButton").play()

	GameNode.removeScene(PlayNode);
	GameNode.addScene(PlayNode,GameNode.playActive);
	GameNode.addScene(MenuNode,GameNode.menuStartGui);
	GameNode.set_gameState(0)
	queue_free()
	pass 