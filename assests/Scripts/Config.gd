extends Node

var HighScore = 0
var Music = true
var Sound = true


func _ready():
	configLoad()
	pass
	
func configSave():
	
	var ConfigData = {
	HighScore=self.HighScore,
	Music=self.Music,
	Sound=self.Sound
	}
	
	var save_config = File.new()
	save_config.open("user://config.save", File.WRITE)
	save_config.store_line(to_json(ConfigData))
	save_config.close()
	pass

func configLoad():
	var load_config = File.new()
	if not load_config.file_exists("user://config.save"):
		return 
	load_config.open("user://config.save", File.READ)
	var data_text = load_config.get_as_text()
	var data_parse = JSON.parse(data_text)
	load_config.close()
	var data = data_parse.result
	self.HighScore=data.HighScore
	self.Music=data.Music
	self.Sound=data.Sound
	pass
	
