extends CanvasLayer

onready var player = get_node("../player")
var timer=0.0;
const TIMER_LIMIT = 2.0
func _ready():
	pass
func _process(delta):
	var Score = player.score
	var Coins = player.coins
	
	get_node("playerHub/VBoxContainer/TextureRect/score").text = String(Score)
	get_node("playerHub/VBoxContainer/TextureRect/coins").text = String(Coins)
	pass
	