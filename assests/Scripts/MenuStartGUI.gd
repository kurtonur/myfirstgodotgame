extends CanvasLayer

onready var Game = get_tree().get_root().get_node("Game")

func _ready():
	$menu/CenterContainer/hScore.text ="High Score\n" + String(Config.HighScore)
	if(Config.Music):
		$menu/CenterContainer3/music.set_pressed(true)
	else:
		$menu/CenterContainer3/music.set_pressed(false)
	pass
	
	if(Config.Sound):
		$menu/CenterContainer4/sound.set_pressed(true)
	else:
		$menu/CenterContainer4/sound.set_pressed(false)
	pass

func _on_start_button_down():
	if(Config.Sound):
		Variables.GameNode.get_node("Sound/soundButton").play()
	Game.set_gameState(1)
	queue_free()
	pass

func _on_music_button_up():
	if(Config.Sound):
		Variables.GameNode.get_node("Sound/soundButton").play()
	Config.Music=!$menu/CenterContainer3/music.is_pressed()
	Config.configSave()
	pass 


func _on_sound_button_up():
	Config.Sound=!$menu/CenterContainer4/sound.is_pressed()
	Config.configSave()
	if(Config.Sound):
		Variables.GameNode.get_node("Sound/soundButton").play()
	pass 
