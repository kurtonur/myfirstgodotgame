extends Node

export(String , FILE, "*.tscn") var playActive
export(String , FILE, "*.tscn") var menuStartGui
export(String , FILE, "*.tscn") var menuRestartGui

var gameState = 0
## 0 menu state
## 1 play state
## 2 pause state
## 3 death state


onready var playNode = get_node("Play")
onready var menuNode = get_node("Menu")


func _process(delta):
	if(Config.Music):
		if(!$Sound/bg.is_playing()):
			$Sound/bg.play(1)
	else:
		$Sound/bg.stop()
	pass

func get_gameState():
	return gameState
	pass
func set_gameState(state):
	gameState = state
	pass

	
func addScene(node : Node,scene : String):
	var loadScene = load(scene)
	var addScene = loadScene.instance()
	node.add_child(addScene)
	pass
	
func removeScene(node : Node):
	for i in range(0, node.get_child_count()):
    node.get_child(i).queue_free()
	pass

func game_restart():
	print("Over")
	gameState = 3
	addScene(menuNode,menuRestartGui)
	pass