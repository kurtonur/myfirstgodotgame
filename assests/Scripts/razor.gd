extends KinematicBody2D

export(float, -1, 1, 0.01)  var rotateSpeed = 0.5

func _ready():
	pass 

func _process(delta):
	$razorSprite.rotate(rotateSpeed)
	pass
