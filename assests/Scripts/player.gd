extends KinematicBody2D

const UP= Vector2(0,-1)
const GRAVITY = 25
const ACCELERATION = 30
const MAX_SPEED = 300
const JUMP_HIGH = -375

var motion = Vector2()
var coins=0
var score=0
var is_HSCheck=true;

signal playerDeath;

onready var Game = get_tree().get_root().get_node("Game")
var gameStarted = 0

func _ready():
	connect("playerDeath",Game,"game_restart")
	pass
	
func _physics_process(delta):
	gameStarted = Game.get_gameState()
	
	if (gameStarted == 1):
		motion.y +=GRAVITY
		score=ceil((get_node("Camera2D").get_camera_position().x-42)/100)
		checkHighScore()
		motion.x = min(motion.x+ACCELERATION,MAX_SPEED)
		$playerSprite.play("run")
		$playerSprite.flip_h = true
		
		if !is_on_floor():
			if motion.y < 0 :
				$playerSprite.play("jumpUp")
			else:
				$playerSprite.play("jumpDown")
		pass
		
	elif(gameStarted==0 || gameStarted==2):
		$playerSprite.play("idle")
		pass
	motion=move_and_slide(motion,UP)
	
	pass

func _input(event):
	if event is InputEventScreenTouch && event.pressed:
		if(gameStarted == 1):
			jump()
		pass
	elif event is InputEventKey && event.pressed:
		if(gameStarted == 1):
			jump()
		pass
	pass
	
func jump():
	if is_on_floor():
		motion.y=JUMP_HIGH
		if($"/root/Config".Sound):
			$Sound/jump.play()
	pass
func _on_Area2D_area_entered(area):
	var layerID = area.collision_layer
	if(( layerID == 4 || layerID == 2) && gameStarted != 3):
		Game.set_gameState(3)
		Variables.Score=score
		Variables.Coins=coins
		saveHighScore()
		emit_signal("playerDeath")
		$playerSprite.play("death")
		if($"/root/Config".Sound):
			$Sound/death.play()
		motion.x=0
		if !is_on_floor():
			motion.y=-JUMP_HIGH;
			pass
		randomize()
		var leftright = ceil(rand_range(0,2))
		
		if(leftright == 1):
			$playerSprite.flip_h=false
			pass
		pass
	elif(layerID == 16):
		if($"/root/Config".Sound):
			$Sound/coin.play()
		coins+=1;
		pass
	pass 

func get_Score():
	return score
	pass
func get_Coins():
	return coins
	pass
func saveHighScore():
	if $"/root/Config".HighScore < score:
		$"/root/Config".HighScore=score
		$"/root/Config".configSave()
	pass
func checkHighScore():
	if(is_HSCheck):
		if $"/root/Config".HighScore < score && $"/root/Config".HighScore != 0 :
			if($"/root/Config".Sound):
				$Sound/Hscore.play()
			is_HSCheck=false
	pass