extends KinematicBody2D

const UP= Vector2(0,-1)
const ACCELERATION = 10
const MAX_SPEED = 300

var motion = Vector2()

onready var player = get_node("../../player")


			
func _physics_process(delta):
	if (player.gameStarted == 1):
		motion.x = min(motion.x+ACCELERATION,MAX_SPEED)
		motion=move_and_slide(motion)
		pass
	
	pass

func _on_Area2D_area_entered(area):
	var layerID = area.collision_layer
	if(layerID == 1):
		if($"/root/Config".Sound):
			$killerSound.play()
		pass
	pass 
