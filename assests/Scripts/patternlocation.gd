extends Node2D

const tileWidth = 640
const patternCount=9
onready var playerCamNOde = get_node("../../player/Camera2D") 



func _ready():
	if(get_child_count() == 0):
		randomize()
		var nextpattern = ceil(rand_range(0,patternCount))
		addScene(self,"res://assests/Scenes/patterns/p"+String(nextpattern)+".tscn")
		pass
	pass 

func _process(delta):
	var camX = playerCamNOde.get_camera_position().x
	var patternX = get_global_position().x
	var patternY = get_global_position().y
	if(camX>=patternX+tileWidth+tileWidth/2):
		removeScene(self)
		randomize()
		var nextpattern = ceil(rand_range(0,patternCount))
		addScene(self,"res://assests/Scenes/patterns/p"+String(nextpattern)+".tscn")
		set_global_position(Vector2(patternX+tileWidth*3,patternY))
		pass 
	pass
	

func addScene(node : Node,scene : String):
	var loadScene = load(scene)
	var addScene = loadScene.instance()
	node.add_child(addScene)
	pass
	
func removeScene(node : Node):
	for i in range(0, node.get_child_count()):
    node.get_child(i).queue_free()
	pass
